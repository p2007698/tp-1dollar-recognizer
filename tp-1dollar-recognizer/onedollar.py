import numpy
import numpy as np
import numpy.linalg as linalg
import matplotlib.pyplot as plt

phi = 0.5 * (-1 + np.sqrt(5))
numPoints = 128


class OneDollar(object):
    """docstring for Recognizer"""

    def __init__(self, angle_range=45., angle_step=2., square_size=250.):
        super(OneDollar, self).__init__()
        self.angle_range = angle_range
        self.angle_step = angle_step
        self.square_size = square_size
        self.templates = []
        self.resampled_templates = []     #for convenience
        self.resampled_gesture = []       #for convenience
        self.labels = []



    #########################################
    # TODO 8
    #
    #########################################
    def recognize(self, points):
        # we apply the transformations on the drawn gesture
        points = self.resample(points, numPoints)
        self.resampled_gesture = points
        points = self.rotateToZero(points)
        points = self.scaleToSquare(points)
        points = self.translateToOrigin(points)

        print('recognize begin', points)
        min_distance = numpy.Infinity
        template_id = -1
        label = "None"
        # score = 0
        denominator =  0.5 * self.square_size * np.sqrt(2)
        display_for_debug = False # only for debug

        for next_index, template in enumerate(self.templates):
            # next_distance = numpy.Infinity
            try:
                print('recognize template before distanceAtBestAngle index =', next_index)
                templateList = template.tolist()
                print(type(points),type(template) )
                next_distance = self.distanceAtBestAngle(points, templateList, -1*self.angle_range, self.angle_range, self.angle_step)
                next_label = self.labels[next_index]
                if (display_for_debug):
                    # ONLY for debug : we display the drawn gesture, the figure of the current template and the calculated distance
                    points_array = numpy.array(points)
                    plt.figure(figsize=(5, 5))
                    plt.clf()
                    plt.scatter(points_array[:, 0], points_array[:, 1], c='lightblue') # display of geasture
                    plt.scatter(points_array[0, 0], points_array[0, 1], c='blue') # first point of geasture in blue
                    plt.scatter(template[:, 0], template[:, 1], c='lightcoral')  # display of the current template
                    plt.scatter(template[0, 0], template[0, 1], c='red')   # first point of current template in red
                    plt.title("distance with " + next_label + " = " + "{:.2f}".format(next_distance)) # calculated distance
                    plt.xlabel('x')
                    plt.ylabel('y')
                    plt.draw()
                    plt.show(block=True)
                print('recognize template after distanceAtBestAngle : next_distance = ', next_distance)
            except Exception as exception:
                print('### Exception', exception)
            if(next_distance < min_distance):
                min_distance,label,template_id = next_distance,next_label,next_index
            # next_index = next_index+1

        score = 1 - min_distance / denominator
        return template_id, label, score


    #########################################
    # Angle values are in degrees
    #########################################
    def distanceAtBestAngle(self, points, template, angle_a, angle_b,
                            angle_step):
        # print("distanceAtBestAngle begin ", points, "template=", template, "angle_a=" , angle_a, "angle_b=" ,angle_b, angle_step)
        x_1 = (phi * angle_a) + ( 1 - phi)*angle_b
        f_1 = self.distanceAtAngle(points, template, x_1)
        x_2 = (1- phi) * angle_a + (phi * angle_b)
        f_2 = self.distanceAtAngle(points, template, x_2)

        while (abs(angle_b-angle_a)>angle_step):
            if (f_1 < f_2) :
                angle_b = x_2
                x_2 = x_1
                f_2 = f_1
                x_1 = (phi * angle_a) + (1 - phi) * angle_b
                f_1 = self.distanceAtAngle(points, template, x_1)
            else :
                angle_a = x_1
                x_1 = x_2
                f_1 = f_2
                x_2 = (1 - phi) * angle_a + (phi * angle_b)
                f_2 = self.distanceAtAngle(points, template, x_2)

        return min(f_1, f_2)
        return min(f_1, f_2)

    ####################
    def distanceAtAngle(self, points, template, angle):
        newPoints = self.rotateBy(points, angle)
        d = pathDistance(newPoints, template)
        return d




    ####################
    def resample(self, points, n):
        # Get the length that should be between the returned points
        path_length = pathLength(points) / float(n - 1)
        newPoints = [points[0]]
        D = 0.0
        i = 1
        while i < len(points):
            point = points[i - 1]
            next_point = points[i]
            d = getDistance(point, next_point)
            if D + d >= path_length:
                delta_distance = float((path_length - D) / d)
                q = [0., 0.]
                q[0] = point[0] + delta_distance * (next_point[0] - point[0])
                q[1] = point[1] + delta_distance * (next_point[1] - point[1])
                newPoints.append(q)
                points.insert(i, q)
                D = 0.
            else:
                D += d
            i += 1
        if len(newPoints) == n - 1:  # Fix a possible roundoff error
            newPoints.append(points[0])
        return newPoints

    ####################
    def fit(self, templates, labels):
        for i, t in enumerate(templates):
            self.addTemplate(t, labels[i])
            #self.labels.append(labels[i])




    ####################
    def addTemplate(self, template, label):
        display_for_debug = False

        points = []
        for i in range(template.shape[0]):
            points.append([template[i,0], template[i,1]])
        if(display_for_debug):
            print('addTemplate initial template ', points)
        points = self.resample(points, numPoints)
        self.resampled_templates.append( points )

        if(display_for_debug):
            # Only for debug : we display the figure after resample operation
            print('addTemplate after  resampled_templates ', points)
            points_array = numpy.array(points)
            plt.scatter(points_array[:,0], points_array[:,1], c='blue')

        points = self.rotateToZero(points)
        if (display_for_debug):
            # Only for debug : we display the figure after rotateToZero operation
            plt.scatter(points[:,0], points[:,1], c='coral')
            print('addTemplate after rotateToZero', points)
        points = self.scaleToSquare(points)
        if (display_for_debug):
            # Only for debug : we display the figure after scaleToSquare operation
            plt.scatter(points[:,0], points[:,1], c='lightgreen')
            print('addTemplate after scaleToSquare', points)
        points = self.translateToOrigin(points)
        if (display_for_debug):
            # Only for debug : we display the figure after translateToOrigin operation
            plt.scatter(points[:,0], points[:,1], c='green')
            print('addTemplate after translateToOrigin', points)
        self.templates.append(points)
        self.labels.append(label)
        if(display_for_debug):
            print("addTemplate end points:", points, "center", np.mean(points,0))
            # Only for debug : we display the figure content after each operation
            plt.title(label)
            plt.xlabel('x')
            plt.ylabel('y')
            plt.show()

    #########################################
    # TODO 6
    #########################################
    def rotateToZero(self, points):
        centroid = np.mean(points, 0)
        # newPoints = points      #remove this line, it is just for compilation
        point0 = points[0]
        print("rotateToZero : centroid = " , centroid, " point0 = ", point0)
        angle = numpy.arctan2(centroid[1] - point0[1], centroid[0] - point0[0])
        print("rotateToZero : angle(radian) = ", angle, " (degree) = ", angle* 180 / np.pi)
        return self.rotateBy(points, -1*angle)

    #########################################
    # TODO 6
    #########################################
    def rotateBy(self, points, angle):
        centroid = np.mean(points, 0)
        newPoints = np.zeros((1, 2))    #initialize with a first point [0,0]
        # todo
        for point in points:
            (cp_x, cp_y) = point - centroid
            q = np.array([0., 0.])
            q[0] = cp_x * np.cos(angle) - cp_y * np.sin(angle) + centroid[0]
            q[1] = cp_x * np.sin(angle) + cp_y * np.cos(angle) + centroid[1]
            # todo 6 update the vector newPoints
            newPoints = np.append(newPoints, [q], 0)
            # print("rotateBy p=", point, ", c=", centroid , "(cp_x,cp_y) = ", cp_x, cp_y, "angle=", angle, " q=", q )
        newPoints = newPoints[1:]       #remove the first point [0,0]
        return newPoints


    #########################################
    # TODO 7
    #########################################
    def scaleToSquare(self, points):
        # todo 7
        points_x = points[:,0]
        points_y =  points[:,1]
        # boundingBox = np.array([0., 0.])
        boundingBox = (np.amax(points_x) - np.amin(points_x)
                 , np.amax(points_y) - np.amin(points_y))
        #print("scaleToSquare points ", points, "boundingBox", boundingBox )
        newPoints = np.zeros((1, 2))    # initialize with a first point [0,0]
        for point in points:
            q = np.array([0., 0.])
            q[0] = point[0] * self.square_size/ boundingBox[0]
            q[1] = point[1] * self.square_size/ boundingBox[1]
            newPoints = np.append(newPoints, [q], 0)
        newPoints = newPoints[1:]      #remove the first point [0,0]
        return newPoints



    ################################
    def translateToOrigin(self, points):
        centroid = np.mean(points, 0)
        newPoints = np.zeros((1, 2))
        self.translation = centroid
        for point in points:
            q = np.array([0., 0.])
            q[0] = point[0] - centroid[0]
            q[1] = point[1] - centroid[1]
            newPoints = np.append(newPoints, [q], 0)
        return newPoints[1:]

    ################################
    def translate(self, points, vec):
        newPoints = np.zeros((1, 2))
        for point in points:
            q = np.array([0., 0.])
            q[0] = point[0] + vec[0]
            q[1] = point[1] + vec[1]
            newPoints = np.append(newPoints, [q], 0)
        return newPoints[1:]



    ####################
    # def score(self, X_test, y_test):
    #     score_ = 0
    #     n_tests = 0
    #     for i, t in enumerate(X_test):
    #         print(i)
    #         points = []
    #         for i in range(t.shape[0]):
    #             points.append([t[i,0], t[i,1]])
    #         t_data, t_id, sc = self.recognize(points)
    #         if (t_id == y_test[i]):
    #             score_ += 1
    #         n_tests += 1
    #     return score_ / n_tests


def pathDistance(path1, path2):
    ''' Calculates the distance between two paths. Fails if len(path1) != len(path2) '''
    if len(path1) != len(path2):
        raise Exception('Path lengths do not match!')
    d = 0
    for p_1, p_2 in zip(path1, path2):
        d = d + getDistance(p_1, p_2)
    return d / len(path1)


def getDistance(point1, point2):
    return linalg.norm(np.array(point2) - np.array(point1))



def pathLength(points):
    length = 0
    for (i, j) in zip(points, points[1:]):
        length += getDistance(i, j)
    return length


def pairwiseIterator(elems):
    for (i, j) in zip(elems, elems[1:]):
        yield (i, j)
    yield (elems[-1], elems[0])
